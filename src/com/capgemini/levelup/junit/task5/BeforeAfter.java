package com.capgemini.levelup.junit.task5;

public class BeforeAfter {

	
	public String method1() {
		System.out.println("Method 1");
		return "method 1";
	}
	
	public String method2() {
		System.out.println("Method 2");
		return "method 2";
	}
}
