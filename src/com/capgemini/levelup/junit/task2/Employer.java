package com.capgemini.levelup.junit.task2;

import com.capgemini.levelup.junit.task2.person.Employee;
import com.capgemini.levelup.junit.task2.person.Manager;

public class Employer {

	
	public Manager addEmployeeToManager(Manager manager, Employee employee) {
		manager.getManagedEmployees().add(employee);
		return manager;
	}
}
