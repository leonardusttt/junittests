package com.capgemini.levelup.junit.task2;

import com.capgemini.levelup.junit.task2.person.Employee;
import com.capgemini.levelup.junit.task2.person.Manager;

public class ManagerFactory {

	
	public Employee getEmployee(String employeeType) {
		
		if (employeeType != null) {
			
			if (employeeType.equalsIgnoreCase("Employee")) {
				return new Employee();
				
			}else if (employeeType.equalsIgnoreCase("Manager")) {
				
				return new Manager();
			}
		}
		return null;
	}
}
