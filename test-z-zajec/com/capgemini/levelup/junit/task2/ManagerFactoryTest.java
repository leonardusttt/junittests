package com.capgemini.levelup.junit.task2;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*; 

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.capgemini.levelup.junit.task2.person.Employee;
import com.capgemini.levelup.junit.task2.person.Manager;

public class ManagerFactoryTest {
    
    private ManagerFactory managerFactory ;
   
    
    @Before
    public void setUp() throws Exception {
        managerFactory = new ManagerFactory();
    }
    
    @Test
    public void shouldReturnEmployeeWhenEmployeeTypeEqualsEmployee() {
        //given
        String employeeType = "employee";
        
        //when
        Employee actualEmployee = managerFactory.getEmployee(employeeType) ;
                                       
        
        boolean isEmployee = actualEmployee instanceof Employee;
        boolean isManager = actualEmployee instanceof Manager;
        
        
        //then
        assertTrue(isEmployee);
        assertFalse(isManager); 
    }
    
    @Test
    public void shouldReturnNullIfEmployeeTypeEqualsNull() {
        //given
        String employeeType = null;
        
        //when
        Employee actualEmployee = managerFactory.getEmployee(employeeType) ;
                                           
        
        //then
        assertNull(actualEmployee);
      
    }
    
    @Ignore
    @Test
    public void ignoredTest() {
        fail("should fail");
    }
    
    @Test
    public void shouldReturnNullIfEmployeeTypeIsIncorrect() {
        //given
        String employeeType = "garbageCollector";
        
        //when
        Employee actualEmployee = managerFactory.getEmployee(employeeType) ;
                                           
        
        //then
        assertNull(actualEmployee);
      
    }
    
    
    //Hamcrest
    
    @Test
    public void shouldReturnEmployeeWhenEmployeeTypeEqualsEmployeeHamcrestTest() {
        //given
        String employeeType = "employee";
        
        //when
        Employee actualEmployee = managerFactory.getEmployee(employeeType) ;
                                           
        
        boolean isEmployee = actualEmployee instanceof Employee;
        boolean isManager = actualEmployee instanceof Manager;
        
        
        //then
        assertThat(isEmployee, is(equalTo(true)));
        assertThat(isEmployee, equalTo(true));
        assertThat(isEmployee, is(true));
        
        assertThat(actualEmployee, instanceOf(Employee.class));
        
        assertThat(isManager, equalTo(false));
        assertThat(isManager, is(false));
        
        assertThat(actualEmployee, not(instanceOf(Manager.class)));
      
    }
    
    
    
    @Test
    public void shouldReturnNullIfEmployeeTypeEqualsNullHamcrestTest() {
        //given
        String employeeType = null;
        
        //when
        Employee actualEmployee = managerFactory.getEmployee(employeeType) ;
                                           
        
        //then
        assertThat(actualEmployee, is(nullValue()));
        assertThat(actualEmployee, nullValue());
      
    }
    
    
    
    
}
